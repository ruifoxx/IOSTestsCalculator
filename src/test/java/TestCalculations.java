import org.junit.Assert;
import org.junit.Test;


public class TestCalculations extends AbstractTest {
    @Test
    public void test5and2equals7 () {
        app.operationsPage().sum5and2();
        Assert.assertTrue(app.operationsPage().isResultCorrect("7"));
    }
}
