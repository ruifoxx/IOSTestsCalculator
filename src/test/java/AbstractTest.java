import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class AbstractTest {
    private static AppiumDriver driver;
    protected static Calculator app;
    private static DesiredCapabilities capabilities = new DesiredCapabilities();

    @BeforeClass
    public static void connect() throws MalformedURLException {
        capabilities.setCapability("platformName", "iOS");
        capabilities.setCapability("platformVersion", "10.3");
        capabilities.setCapability("deviceName", "iPhone 7");
        capabilities.setCapability("app", "https://s3.amazonaws.com/appium/TestApp8.4.app.zip");
        driver = new IOSDriver(new URL("http://127.0.0.1:4723/wd/hub/"), capabilities);
        app = new Calculator(driver);
    }

    @AfterClass
    public static void disconnect() {
        driver.closeApp();
    }
}
