import io.appium.java_client.AppiumDriver;
import page.OperationsPage;

public class Calculator {

    private final AppiumDriver driver;

    public Calculator(AppiumDriver driver) {
        this.driver = driver;
    }

    public OperationsPage operationsPage() {
        return new OperationsPage(this.driver);
    }
}