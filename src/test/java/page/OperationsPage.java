package page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OperationsPage extends AbstractPage {
    @iOSFindBy(id = "IntegerA")
    private MobileElement intAField;

    @iOSFindBy(id = "IntegerB")
    private MobileElement intBField;

    @iOSFindBy(id = "ComputeSumButton")
    private MobileElement compSumBtn;

    @iOSFindBy(id = "Answer")
    private MobileElement answerField;

    public OperationsPage(AppiumDriver driver) {
        super(driver);
    }

    private void fillIntA(String intA) {
        intAField.sendKeys(intA);
    }

    private void fillIntB(String intB) {
        intBField.sendKeys(intB);
    }

    private void clickCompBtn() {
        compSumBtn.click();
    }

    public void sum5and2() {
        fillIntA("5");
        fillIntB("2");
        clickCompBtn();
    }


    public boolean isResultCorrect(String result) {
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.textToBePresentInElement(answerField, result));
        return true;

    }
}